#include <Arduino.h>
#include "PMS.h"
#include <SoftwareSerial.h>

# if defined (ARDUINO_T_Beam)
#   define ESP32_ARCH 1
#   define PMS_RX_PIN 13 // Rx from PMS (== PMS Tx)
#   define PMS_TX_PIN 2 // Tx to PMS (== PMS Rx)
# elif defined (ARDUINO_ARCH_ESP32) || defined(ESP32)
#   define ESP32_ARCH 1
#   define PMS_RX_PIN 22 // Rx from PMS (== PMS Tx)
#   define PMS_TX_PIN 21 // Tx to PMS (== PMS Rx)
# elif defined(ARDUINO_ARCH_ESP8266)
#   define PMS_RX_PIN D4 // Rx from PMS (== PMS Tx)
#   define PMS_TX_PIN D2 // Tx to PMS (== PMS Rx)
# else
# 	error "Architecture unknown and not supported"
# endif

#define DEEP_SLEEP_SECONDS 30

SoftwareSerial pmsSerial(PMS_RX_PIN, PMS_TX_PIN); // Rx pin = GPIO2 (D4 on Wemos D1 Mini)
PMS pms(pmsSerial);
PMS::DATA data;

uint32_t snap_time = 0;

#define STARTING 0
#define WAKING 1
#define SLEEPING 2
uint8_t state = STARTING;

void setup()
{
  Serial.begin(9600);   // GPIO1, GPIO3 (TX/RX pin on ESP-12E Development Board)
  pmsSerial.begin(9600);   // Connection for PMSx003
  pms.passiveMode();    // Switch to passive mode
  Serial.println("\nInitialized");
}

void loop()
{
  uint32_t current_time = millis();

  //TODO: investigate why without this constant writting 
  //      to serial the PMS don't go to sleep mode on esp32 boards
  Serial.print("current[");
  Serial.print(current_time);
  Serial.print("] - snap[");
  Serial.print(snap_time);
  Serial.print("]: ");
  Serial.print(current_time - snap_time);
  Serial.print(", ");

  if (state == STARTING && current_time - snap_time >= 30000) {
    Serial.println("Waking up, wait 30 seconds for stable readings...");
    pms.wakeUp();
    state = WAKING;
    snap_time = millis();
    current_time = snap_time;
  }

  if (state == WAKING && current_time - snap_time >= 30000) {
    Serial.println("Send read request...");
    pms.requestRead();
    Serial.println("Wait max. 1 second for read...");
    if (pms.readUntil(data)) {
      Serial.print("PM 1.0 (ug/m3): ");
      Serial.println(data.PM_AE_UG_1_0);

      Serial.print("PM 2.5 (ug/m3): ");
      Serial.println(data.PM_AE_UG_2_5);

      Serial.print("PM 10.0 (ug/m3): ");
      Serial.println(data.PM_AE_UG_10_0);
    } else {
      Serial.println("No data.");
    }
    Serial.println("putting PMS to sleep");
    pms.sleep();
    pmsSerial.flush();
    state = SLEEPING;
    snap_time = millis();
    current_time = snap_time;
  }

  if (state == SLEEPING && current_time - snap_time >= 5000) {
    Serial.printf("deep sleep esp for %d seconds", DEEP_SLEEP_SECONDS);
# if defined(ESP32_ARCH)
    esp_sleep_enable_timer_wakeup(DEEP_SLEEP_SECONDS * 1000000);
    esp_deep_sleep_start();
# elif defined(ARDUINO_ARCH_ESP8266)
    // Connect pin D0 to the RESET pin of the board for this to work
    ESP.deepSleep(DEEP_SLEEP_SECONDS * 1000000); 
# endif
  }

}
